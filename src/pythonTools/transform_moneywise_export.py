#! /usr/bin/python3
import csv
from dateutil.parser import parse, parserinfo

# rows:
# 0: Date
# 1: Account
# 2: Main Category (Main Cat.)
# 3: Subcategory (Sub Cat.)
# 4: Contents
# 5: Amount
# 6: Income/Expense (Inc./Exp.)
# 7: Details

with open('../../data/moneywise/MoneyWise.csv') as csv_source:
	with open('../../data/moneywise/import.txt', mode='w', newline='', encoding='utf-16') as csv_target:
		csv_reader = csv.reader(csv_source, delimiter=';')
		csv_writer = csv.writer(csv_target, delimiter='\t')
		current_line = 0
		for row in csv_reader:
			print(f'Current line: {current_line}')
			# Ignore non-transactional entries
			if row[4] == 'Set account balance':
				current_line += 1
				continue

			transformed_row = row.copy()

			# overwrite row names
			if current_line == 0:
				transformed_row[2] = "Main Cat."
				transformed_row[3] = "Sub Cat."
				transformed_row[6] = "Inc./Exp."
				csv_writer.writerow(row)
				current_line += 1
				continue

			# transform date
			date = parse(transformed_row[0])
			transformed_row[0] = f'{date.strftime("%Y")}.{date.strftime("%m")}.{date.strftime("%d")}'

			# rename accounts
			account = row[1]
			if account == 'Bargeld':
				transformed_row[1] = 'Cash'
			elif account == 'Ing DiBa Depot':
				transformed_row[1] = 'Ing Depot'

			# split category into main and sub
			category = row[2]
			main_category = category.split(':')[0].strip()
			transformed_row[2] = main_category
			if category.split(':').__len__() > 1:
				subcategory = category.split(':')[1].strip()
			else:
				subcategory = ''
			transformed_row[3] = subcategory

			# extract shop/seller from contents field if exists
			contents = row[4]
			if contents.split('-').__len__() > 1:
				transformed_row[4] = contents.split('-')[0].strip()
				transformed_row[7] = contents.split('-')[1].strip()

			# transform amount
			amount = float(row[5])
			transformed_row[5] = int(abs(amount))

			# write to file
			csv_writer.writerow(transformed_row)
			current_line += 1





