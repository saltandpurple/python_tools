from tkinter import *
from tkinter import filedialog
from pathlib import Path
import os


class VidExtractor:
	sourceDirectory = "C:/OneDrive/Pictures/chicks"  # default
	destinationDirectory = "C:/Users/d0gho/Desktop/vids"  # default

	def __init__(self):
		root = Tk()
		root.geometry("500x500")
		self.modeSelection = IntVar(value=1)
		self.source_select_button = Button(root, text="Select source", command=lambda: self.select_directory("source")).pack()
		self.destination_select_button = Button(root, text="Select destination", command=lambda: self.select_directory("destination")).pack()
		self.label = Label(root, text="")
		self.execute_button = Button(root, text="Start extraction", command=self.extract).pack()
		root.mainloop()

	def select_directory(self, directory):
		if directory == "source":
			self.sourceDirectory = filedialog.askdirectory(title="Select source")
		else:
			self.destinationDirectory = filedialog.askdirectory(title="select destination")

	def extract(self):
		print("starting extraction.")
		print("source: " + self.sourceDirectory)
		print("destination: " + self.destinationDirectory)
		vids = []
		if self.sourceDirectory != "" and self.destinationDirectory != "":
			for root, dirs, files in os.walk(self.sourceDirectory):
				for file in files:
					if file[len(file) - 3:len(file)] == "avi" or file[len(file) - 3:len(file)] == "mp4":
						vids.append(os.path.join(root, file))
						# print("Video found: " + os.path.join(root,file))

		print("Found " + len(vids).__str__() + " videos. Moving them now...")
		for video in vids:
			new_path = self.destinationDirectory + video[len(self.sourceDirectory):]
			os.makedirs(os.path.dirname(new_path), exist_ok=True)
			if not Path(new_path).exists():
				os.rename(video, new_path)
			else:
				# os.remove(video)
				print('deleted ' + video)

		print("Moved " + len(vids).__str__() + " videos sucessfully.")
		exit(1)


VidExtractor()
