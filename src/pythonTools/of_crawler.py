import json
import sys
import os
import math
import urllib.request
import requests
import time
from collections import namedtuple
from collections import OrderedDict

import uncurl


def find_param(search_term: str, search_mass: str, is_header: bool = False) -> str:
	offset = 2 if is_header else 1
	start_pos = search_mass.find(search_term)
	start_pos = start_pos + len(search_term) + offset  # add offset to the length of the search term to account for : or =
	end_pos = search_mass.find('"', start_pos)
	return search_mass[start_pos:end_pos]


def json_to_tuples_decoder(of_dict):
	return namedtuple("post", of_dict.keys(), rename=True)(*of_dict.values())


def set_params(url: str, hardcoded_params: dict = {}):
	order = ['limit', 'order', 'skip_users', 'skip_users_dups', 'beforePublishTime', 'format']
	params = {}

	# parse url for params
	start_index = url.find('?') + 1
	end_index = url.find('&')
	if end_index == -1:
		end_index = len(url)
	while start_index < len(url):
		param = url[start_index:end_index]
		param_name = param[0:param.find('=')]
		param_value = param[param.find('=') + 1:len(param)]

		params[param_name] = param_value

		start_index = end_index + 1
		end_index = url.find('&', start_index)
		if end_index == -1:
			end_index = len(url)

	# overwrite with hardcoded params (if any)
	for key in hardcoded_params:
		params[key] = hardcoded_params[key]

	# sort by the order set above
	for key in order:
		if key in params.keys():
			params[key] = params.pop(key)
	return OrderedDict(params)


def clean_url(url: str):
	return url[0:url.find('?')]


def parse_cookies(cookies):
	cookie_string = ''
	for key in cookies:
		cookie_string += key
		cookie_string += '='
		cookie_string += cookies[key]
		cookie_string += '; '
	return cookie_string


def get_media_urls(context, url, type):
	print(f'Crawling {type}...')
	if type == 'pictures':
		if 'photos' not in context.url:
			url = context.url[0 : context.url.find('posts') + 5] + '/photos' + context.url[context.url.find('posts') + 5 : len(context.url)]
	if type == 'videos':
		if 'videos' not in context.url:
			url = context.url[0 : context.url.find('posts') + 5] + '/videos' + context.url[context.url.find('posts') + 5 : len(context.url)]

	media = []
	postCount = 0
	params = set_params(url, hardcoded_params={'beforePublishTime': '1620529890.000000'})
	headers = context.headers
	cookies = context.cookies
	url_clean = clean_url(url)

	response = requests.get(url_clean, params=params, headers=headers, cookies=cookies).text
	posts = json.loads(response, object_hook=json_to_tuples_decoder)

	# as long as there are further posts
	while posts.hasMore:
		postCount += len(posts.list)
		print(f'Found {postCount} posts so far.')
		for post in posts.list:
			for medium in post.media:  # go through list of attached media
				if medium.source.source is not None:  # only if url is not null
					media.append(medium)

		posted_at = posts.list[len(posts.list) - 1].postedAtPrecise
		params = set_params(url, hardcoded_params={"beforePublishTime": posted_at})
		response = requests.get(url_clean, params=params, headers=headers, cookies=cookies).text
		posts = json.loads(response, object_hook=json_to_tuples_decoder)

	print(f'Finished crawling. {len(media)} {type} found!')
	return media


def download_media(media, type, account_name):
	number_of_media = len(media)
	count = 1
	download_directory = f'../data/of_crawler/{account_name}/{type}'
	# ensure directory exists
	if not os.path.exists(download_directory):
		os.makedirs(download_directory)

	print(f'Starting download of {number_of_media} {type}...')
	for entry in media:
		print(f'Downloading {count} of {number_of_media} {type}...')
		media_url = entry.source.source
		filename = media_url[media_url.rfind('/') + 1: media_url.rfind('?')]
		urllib.request.urlretrieve(media_url, f'{download_directory}/{filename}')
		count = count + 1

	print(f'Successfully downloaded {number_of_media} {type}.')


#######################################################################################################################
####################################### SCRIPT EXECUTION STARTS HERE ##################################################
#######################################################################################################################


# if no argument is given (or the script is not called via cli) use the curl provided here. This provides an
# alternative mode of running the script directly out of the IDE.
if len(sys.argv) > 1:
	curlImport = sys.argv[1]
else:
	# curlImport = 'curl "https://onlyfans.com/api2/v2/users/4641748/posts/photos?limit=10&order=publish_date_desc' \
	#              '&skip_users=all&skip_users_dups=1&pinned=0&app-token=33d57ade8c02dbc5a333db99ff9ae26a" -H ' \
	#              '"User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:82.0) Gecko/20100101 Firefox/82.0" -H ' \
	#              '"Accept: application/json, text/plain, */*" -H "Accept-Language: en,de;q=0.5" --compressed -H ' \
	#              '"Referer: https://onlyfans.com/natalee/photos" -H "access-token: 7fs8umb4ivumetojgegm3bpbrm" -H ' \
	#              '"user-id: 10042081" -H "x-bc: 080712e6b8585b90b8ca44c1676bac1ea1b11b22" -H "time: 1600963669778" -H ' \
	#              '"sign: b7525b4bc49e7f508eee0ef9bae59ce66aa1b4cb" -H "DNT: 1" -H "Connection: keep-alive" -H "Cookie: ' \
	#              'sess=7fs8umb4ivumetojgegm3bpbrm; csrf=gzrNgZX1fee03f4a392290d838d0bb2c59e04aac; ref_src=; ' \
	#              'fp=60cd1fe1543e62fc51fb8f986962e9b0; auth_id=10042081" -H "Pragma: no-cache" -H "Cache-Control: ' \
	#              'no-cache" -H "TE: Trailers"'
	curlImport = 'curl "https://onlyfans.com/api2/v2/users/19046397/posts/photos?limit=10&order=publish_date_desc&skip_users=all&skip_users_dups=1&beforePublishTime=1613368208.000000&format=infinite" -H "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:89.0) Gecko/20100101 Firefox/89.0" -H "Accept: application/json, text/plain, */*" -H "Accept-Language: en,de;q=0.5" --compressed -H "Referer: https://onlyfans.com/allipark22/photos" -H "app-token: 33d57ade8c02dbc5a333db99ff9ae26a" -H "user-id: 10042081" -H "x-bc: de8258493d5da80ef5eb2a9f4e8b569a129fd925" -H "time: 1620746021505" -H "sign: 7:a09b345b26e6f6514760fc6b7919f8429ce94567:490:609a9c7b" -H "DNT: 1" -H "Connection: keep-alive" -H "Cookie: fp=a5bf99a3a9f72e74ff6d09da82f6416d; sess=2hd5htk86pcsfgp2lgpcmu19ho; csrf=01ALvLUv80605ba1405da559250f56dbb921fa67; ref_src=; auth_id=10042081; st=ae3d9a9753c83b8db5b9c9c1b75382717e56df9d9e56fececd0dffb96d760379; cookiesAccepted=true; dark_mode=true" -H "Sec-GPC: 1" -H "TE: Trailers"'

account_name = "allisonparker"
context = uncurl.parse_context(curlImport)

url = context.url
# url = curlImport[curlImport.find('https'):curlImport.find('posts') + 5]
#
# params = {
# 	'limit'          : '10',
# 	'order'          : 'publish_date_desc',
# 	'skip_users'     : 'all',
# 	'skip_users_dups': '1',
# 	'pinned'         : '0',
# 	'format'         : 'infinite'
# }
#
# headers = {'Accept': 'application/json, text/plain, */*'}
# headers['app-token'] = find_param('app-token', curlImport, True)
# headers['User-Agent'] = find_param('User-Agent', curlImport, True)
# headers['access-token'] = find_param('access-token', curlImport, True)
# headers['Cookie'] = find_param('Cookie', curlImport, True)

pictures = get_media_urls(context, url, "pictures")
download_media(pictures, "pictures", account_name)
videos = get_media_urls(context, url, "videos")
download_media(videos, "videos", account_name)
