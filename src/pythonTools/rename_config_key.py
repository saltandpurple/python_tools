filePath: str = '../data/macos.xml'

with open(filePath, "rt") as settingsIn:
	settings: str = settingsIn.read()

# print(settings)
settings = settings.replace("shift", "mac-key")
print(settings)

with open(filePath, "wt") as settingsOut:
	settingsOut.write(settings)

