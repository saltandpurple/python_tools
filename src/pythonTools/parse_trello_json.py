import json
import sys
import os
from collections import namedtuple


def custom_trello_decoder(trello_dict):
	return namedtuple("trello", trello_dict.keys(), rename=True)(*trello_dict.values())


# if no argument is given (or the script is not called via cli) extract all lists, otherwise only the one specified
if len(sys.argv) > 1:
	listToExtract = sys.argv[1]
else:
	listToExtract = "use_all"

with open('../data/trello.json', 'r', encoding="utf_8") as jsonFile:
	jsonString = jsonFile.read()
	trelloBoard = json.loads(jsonString, object_hook=custom_trello_decoder)

	# take each list from the board, find the matching cards and write their names to a file named like the list
	for trelloList in trelloBoard.lists:
		# create result directory if it doesn't exist
		if not os.path.exists('../data/results'):
			os.makedirs('../data/results')

		# escape filenames
		fileName: str = trelloList.account_name
		fileNameEscaped = fileName.translate(str.maketrans({
															"/": r"-",
															".": r"-"
															}))
		resultFile = open('../data/results/' + fileNameEscaped + ".txt", 'w', encoding="utf_8")

		for card in trelloBoard.cards:
			if card.idList == trelloList.id:
				resultFile.write(card.account_name)
				resultFile.write('\r')
		resultFile.close()
