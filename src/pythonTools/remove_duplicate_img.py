from tkinter import *
from tkinter import filedialog
from skimage.metrics import structural_similarity
import os
import cv2


class ImageCleaner:
	directory = ""
	duplicates = []

	def __init__(self):
		root = Tk()
		self.modeSelection = IntVar(value=1)
		Label(root, text="Choose the attribute for duplicate detection:", justify=LEFT).pack()
		Radiobutton(root, text="By name", variable=self.modeSelection, value=1).pack(anchor=W)
		Radiobutton(root, text="By picture", variable=self.modeSelection, value=2).pack(anchor=W)
		self.select_button = Button(root, text="Select folder", command=self.select_directory).pack()
		self.label = Label(root, text="")
		self.delete_button = Button(root, text="Remove duplicates", command=self.delete_duplicates)

		root.mainloop()

	def select_directory(self):
		self.directory = filedialog.askdirectory(title="Select folder")
		self.duplicates = []
		self.find_duplicates(self.modeSelection.get())
		self.label['text'] = len(self.duplicates).__str__() + " duplicates found"
		if len(self.duplicates) > 0:
			self.label.pack()
			self.delete_button.pack()

	def find_duplicates(self, mode=1):
		for root, dirs, files in os.walk(self.directory):
			for file in files:
				if mode == 1:
					ImageCleaner.compare_by_name(root, file, files, self.duplicates)
				if mode == 2:
					ImageCleaner.compare_by_content(root, file, files, self.duplicates)

	@staticmethod
	def compare_by_name(root, original, files, duplicates: list):
		file_without_ending = original[:len(original) - 4]  # cut off file type ending
		matches = [file for file in files if file_without_ending in file]
		if matches.__len__() > 1:
			matches.remove(original)
			for match in matches:
				duplicates.append(os.path.join(root, match))
		return duplicates

	@staticmethod
	def compare_by_content(root, original, files, duplicates: list):
		print("Searching for duplicates of " + original + " ...")
		count = 0
		original_image = cv2.imread(os.path.join(root, original))
		for file in files:
			file_path = os.path.join(root, file)
			duplicate_image = cv2.imread(file_path)
			if original != file and ImageCleaner.is_identical_image(original_image, duplicate_image):
				duplicates.append(file_path)
				print(file_path)
				count += 1
		print("Found " + str(count) + " duplicates.")
		return duplicates

	# expand on comparison method
	@staticmethod
	def is_identical_image(img1, img2) -> bool:
		gray1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
		gray2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
		(score, diff) = structural_similarity(gray1, gray2, full=True)
		diff = (diff * 255).astype("uint8")
		print(img1 + " and " + img2 + "  have a similarity of: " + score)
		return FALSE


# simple direct comparison
	# if img1.shape == img2.shape:
	# 	difference = cv2.subtract(img1, img2)
	# 	blue, green, red = cv2.split(difference)
	# 	return cv2.countNonZero(blue) == 0 and cv2.countNonZero(green) == 0 and cv2.countNonZero(red) == 0


	def delete_duplicates(self):
		deleted = []
		print("Deleting " + len(self.duplicates).__str__() + " duplicates...")
		for duplicate in self.duplicates:
			if os.path.exists(duplicate):
				os.remove(duplicate)
				deleted.append(duplicate)
		print("Successfully deleted " + len(deleted).__str__() + " duplicates")


ImageCleaner()
