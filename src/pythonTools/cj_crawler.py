from bs4 import BeautifulSoup
import requests
import os


def crawl_page(baseurl: str):
	# create and switch to directory
	page_name = baseurl[baseurl.rfind('/') + 1:]
	if not os.path.exists(page_name):
		os.makedirs(page_name)
	os.chdir(page_name)
	print(f'Downloading to {os.getcwd()}')

	# download and parse page
	print('Starting to crawl ' + baseurl + ' ...')
	basepage = requests.get(baseurl).text
	soup = BeautifulSoup(basepage, 'html.parser')
	articles = soup.find_all("article")

	subpage_count = len(articles)
	global_counter = 1
	print(f'{subpage_count} subpages found.')
	for i, article in enumerate(articles):
		print(f'Crawling subpage {i+1} of {subpage_count}...')
		global_counter = crawl_subpage(article.a['href'], page_name, global_counter)

	print(f'Download completed. Successfully downloaded {global_counter} images.')
	os.chdir('..')


def crawl_subpage(url: str, filename: str, global_counter: int):
	page = requests.get(url).text
	soup = BeautifulSoup(page, 'html.parser')

	#  get main picture url - if it's not called 'main.jpg', this is not a crawlable page for our purposes, so abort
	main_image = soup.find('article').find('img')
	if main_image is None or main_image['src'].find('main.jpg') == -1:
		print('Subpage is not suitable for download (no main.jpg). Aborting crawl.')
		return global_counter

	# get links to the image pages and crawl the image links from them
	page_name = url[url.rfind('/') + 1:]
	images = [main_image]
	tables = soup.find_all('table')
	for table in tables:
		for atag in table.find_all('a'):
			crawl_image_page(atag['href'], images)
	print(f'Found {len(images)} images on subpage {page_name}. Starting download...')

	# download that shit!
	for i, image in enumerate(images):
		print(f'Downloading image {i+1} of {len(images)} at URL {image}...')
		downloaded_image = requests.get(image).content
		with open(f'{filename}_{global_counter}.jpg', 'wb') as file:
			file.write(downloaded_image)
		global_counter = global_counter + 1

	print(f'Successfully downloaded {len(images)} images. Crawling of subpage completed.')
	return global_counter


def crawl_image_page(url: str, images: list):
	page_html = requests.get(url).text
	image = BeautifulSoup(page_html, 'html.parser').find('img')['src']
	image_url = url[:url.rfind('/') + 1] + image
	if not image.__contains__('logo.png'):  # exclude irrelevant logos
		images.append(image_url)


# 'https://celebjihad.com/sara-underwood','https://celebjihad.com/demi-rose',
#             'https://celebjihad.com/anna-nystrom', 'https://celebjihad.com/megan-fox3',
#             'https://celebjihad.com/nicki-minaj', 'https://celebjihad.com/selena-gomez2',
baseurls = ['https://celebjihad.com/ana-de-armas2']

directory = '../data/cjcrawler/'
if not os.path.exists(directory):
	os.makedirs(directory)
os.chdir(directory)

for baseurl in baseurls:
	crawl_page(baseurl)

